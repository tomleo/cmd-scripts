# This is a proof-of-concept for generating hash values of all files in a
# tree. This file can then be diffed against another directory to check
# for differences.

import os
import hashlib

# hash_bytestr_iter and file_as_blockiter via
# http://stackoverflow.com/a/3431835/465270
def hash_bytestr_iter(bytesiter, hasher, ashexstr=False):
    for block in bytesiter:
        hasher.update(block)
    return (hasher.hexdigest() if ashexstr else hasher.digest())

def file_as_blockiter(afile, blocksize=65536):
    with afile:
        block = afile.read(blocksize)
        while len(block) > 0:
            yield block
            block = afile.read(blocksize)

def recurse_directory(directory_path):
    for root, dirs, files in os.walk(directory_path):
        if root == '_build':
            continue
        # print "%s/" % root
        for f in files:
            _file = os.path.join(root, f)
            _hash = hashlib.sha256()
            with open(_file, 'rb') as fin:
                print "%s%s" % (root, f)
                # print "%s%s,%s" % (root, f, hash_bytestr_iter(file_as_blockiter(fin), _hash, ashexstr=True))


if __name__ == '__main__':

    # print "> os.path.expanduser('~'):"
    # print os.path.expanduser('~')
    # print "> __file__:"
    # print __file__
    # print "> os.path.abspath(__file__):"
    # print os.path.abspath(__file__)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', type=str)
    fpath = parser.parse_args().dir
    recurse_directory(fpath)

    # home_directory = os.path.join(os.path.expanduser('~'), 'energysage', 'es-site', 'docs')
    # recurse_directory(home_directory)

    # https://pymotw.com/2/os/
    # https://pymotw.com/2/ospath/

