alias l="ls -lsh --hide='*.pyc' --color=never"
alias xclip="xclip -selection c"
alias dir="pwd | tr -d '\n' | xclip"
alias git-gui="git-cola"
alias lspyc="find ./ -name \*.pyc -ls"
alias rmpyc="find ./ -name \*.pyc -exec rm {} \;"
alias b='git for-each-ref --count=10 --sort=-committerdate refs/heads/ --format="%(refname:short)"'

# Decrypt File to Directory
alias openvault="openssl enc -aes-256-cbc -d -in ~/vault.tar.gz.dat | tar xz; thunar ~/vault"

# Encrypt Directory to File
alias closevault="tar cz vault/ | openssl enc -aes-256-cbc -out ~/vault.tar.gz.dat; rm -r ~/vault"

alias picture="viewnior"
alias lgvim="gvim -c 'Prosession ~/Dropbox'"
alias openwork="veracrypt /home/tom/work/content.enc /media/work/content"
alias closework="veracrypt -d"
alias aenv="source ./env/bin/activate"

# Project specific aliases go below
# alias eslog="cd /home/tom/energysage && tail -f local_es.log"
alias eslog="cd /home/tom/energysage && unbuffer tail -f local_es.log | awk -f /home/tom/hacking/cmd-scripts/djdev.awk"

