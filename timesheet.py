import re
import datetime
from collections import namedtuple

# TOKEN GROUPS
OPT_SPACE = r'(\ +)?'
DEC = r'([\d|\.]+)'
PER = r'([\d|\.]+%)'
DAY = r'([MTWRFS])'
COL_SEP = r'(\|)'
HOZ_SEP = r'(-+)'

H_DAY = r'(D)'
H_TIME = r'(Time)'
H_SUM = r'(Sum)'
H_DONE = r'(%\ done)'

START_COL = r'^' + COL_SEP
END_COL = COL_SEP + r'$'
PADDED_TOKEN = r'%(OPT_SPACE)s%(TOKEN)s%(OPT_SPACE)s'
TOKEN_OR_EMPTY = r'(%(PADDED_TOKEN)s)|([\ ]+)'


def pad_token(token):
    return PADDED_TOKEN % {'OPT_SPACE': OPT_SPACE, 'TOKEN': token}


def token_or_empty(token):
    return TOKEN_OR_EMPTY % dict(OPT_SPACE=OPT_SPACE,
                                 PADDED_TOKEN=pad_token(token))


TokenMatch = namedtuple('TokenMatch', ['self', 'token', 'matches'])


class Token(object):
    line_regex = None

    def __init__(self):
        if hasattr(self, 'build_pattern'):
            self.line_regex = self.build_pattern()
        self.line_pattern = re.compile(self.line_regex)

    def line_match(self, token):
        return TokenMatch(self, token, self.line_pattern.search(token))

    def tokenize(self, token_match):
        token_names = [i[0] for i in self.regex_map()]
        match_groups = token_match.matches.groups()
        match_groups = [i for i in match_groups if not re.search(r'^([\ ]+)$', i)]
        return (self, zip(token_names, match_groups))


class HeadToken(Token):
    """
    | D | Time | Sum  | % done |
    """
    def regex_map(self):
        return [('start_col', START_COL),
                ('h_day', pad_token(H_DAY)),
                ('col_sep', COL_SEP),
                ('h_time', pad_token(H_TIME)),
                ('col_sep', COL_SEP),
                ('h_sum', pad_token(H_SUM)),
                ('col_sep', COL_SEP),
                ('h_done', pad_token(H_DONE)),
                ('end_col', END_COL)]

    def build_pattern(self):
        return ''.join([i[1] for i in self.regex_map()])


class SperatorToken(Token):
    """
    |---|------|------|--------|

    """
    def regex_map(self):
        return [
            ('start_col', START_COL),
            ('hoz_sep', HOZ_SEP),
            ('col_sep', COL_SEP),
            ('hoz_sep', HOZ_SEP),
            ('col_sep', COL_SEP),
            ('hoz_sep', HOZ_SEP),
            ('col_sep', COL_SEP),
            ('hoz_sep', HOZ_SEP),
            ('end_col', END_COL)
        ]

    def build_pattern(self):
        return ''.join([i[1] for i in self.regex_map()])


class RowToken(Token):
    """
    | S | 0.0  | 0    | 0%     |
    """
    def regex_map(self):
        return [
            ('start_col', START_COL),
            ('day', token_or_empty(DAY)),
            ('col_sep', COL_SEP),
            ('dec', token_or_empty(DEC)),
            ('col_sep', COL_SEP),
            ('dec', token_or_empty(DEC)),
            ('col_sep', COL_SEP),
            ('per', token_or_empty(PER)),
            ('end_col', END_COL)
        ]

    def build_pattern(self):
        return ''.join([i[1] for i in self.regex_map()])

    def tokenize(self, token_match):
        token_names = [i[0] for i in self.regex_map() if 'col' not in i[0]]
        data = [i.strip() for i in token_match.token.split('|') if i]
        return (self, zip(token_names, data))


Row = namedtuple('Row', ['day', 'time', 'sum', 'per'])


class ParseTable(object):

    def __init__(self, md_table):
        self.head_token = HeadToken()
        self.seperator_token = SperatorToken()
        self.row_token = RowToken()

        self.rows = []
        self.md_table = md_table
        self.parse_md_table()

    def tokens(self):
        return [self.head_token, self.seperator_token, self.row_token]

    def token_types(self):
        return [t.__class__.__name__ for t in self.tokens()]

    def handle_line_type(self, line):
        for token_class in self.tokens():
            # print(token_class)
            match = token_class.line_match(line)
            if match.matches:
                return token_class.tokenize(match)

    def populate_row_data(self, line_no, token_class, token_obj):
        if token_class is self.row_token:
            self.rows.append(Row(*[j for i, j in token_obj]))

    def parse_md_table(self):
        if self.md_table:
            for line_no, line in enumerate(self.md_table.split('\n')):
                line_tuple = self.handle_line_type(line)
                if line_tuple:
                    self.populate_row_data(line_no, *line_tuple)

    def get_day(self, date):
        return self.rows[date.weekday()]

    def set_day(self, date, row):
        self.rows[date.weekday()] = row

    def set_time(self, date, time):
        _day = self.get_day(date)

        return Row(_day.day, time, 'asdf', 5)
        # import pdb; pdb.set_trace()
        # _day.time = time


def md_table_to_py(S):
    pt = ParseTable(S)
    today = datetime.date.today()
    print(pt.rows)
    print(pt.set_time(today, 5.8))
    print(pt.rows)


def get_md_string(fname):
    with open(fname, 'r+') as f:
        return f.read()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Manipulate markdown table data')
    parser.add_argument('-f', '--fname', required=True, type=str)
    kwargs = vars(parser.parse_args())
    fname = kwargs.get('fname')

    S = get_md_string(fname)
    md_table_to_py(S)

