#!/usr/bin/env python3
import os
from collections import namedtuple
from operator import itemgetter

import acoustid
import humanfriendly
import mutagen
from mutagen.mp3 import MP3, BitrateMode
from mutagen.easyid3 import EasyID3

class _AudioFormat(dict):
    def __getattr__(self, item):
        return super(_AudioFormat, self).get(item.lower())

AudioFormat = _AudioFormat(
    mp3 = 'mp3',
    flac = 'flac'
)


class Song:
    _attr_names = ['tracknumber', 'artist', 'date', 'album', 'title'] #'disp_fmt']

    def __init__(self, dir_, file_, **kwargs):
        self.dir = dir_
        self.file = file_
        self.file_loc = os.path.join(self.dir, self.file)
        self.acoustic_key = kwargs.get('acoustic_key', None)

        ext = os.path.splitext(file_)[1][1:]
        if ext == AudioFormat.MP3:
            self.format = AudioFormat.MP3
            self.set_audio_mp3()
        elif ext == AudioFormat.FLAC:
            self.format = AudioFormat.FLAC
            self.set_audio_flac()
        self.populate()
        info_missing = self.info_missing()
        if info_missing and self.acoustic_key:
            self.lookup_acoustic_id(info_missing)

    def set_audio_mp3(self):
        self.audio = MP3(self.file_loc, ID3=EasyID3)
        meta = MP3(self.file_loc)
        bitrate = int(meta.info.bitrate / 1000)
        if meta.info.bitrate == BitrateMode.VBR:
            self.mode = 'VBR'
            self.bitrate = '%s %s' % (bitrate, mode)
        elif meta.info.bitrate == BitrateMode.ABR:
            self.mode = 'ABR'
            self.bitrate = '%s %s' % (bitrate, mode)
        else:
            self.mode = 'CBR'  # Most MP3's should be 320 CBR
            self.bitrate = '%s' % (bitrate)

    def set_audio_flac(self):
        #TODO: determine if 16bit or 24bit FLAC Lossless
        self.mode = 'Lossless'
        self.bitrate = 'Flac'
        self.audio = mutagen.File(self.file_loc)

    def add(self, name):
        try:
            value = self.audio.get(name, [])[0]
        except Exception as exp:
            return
        setattr(self, name, value)

    def populate(self):
        for a in self._attr_names:
            self.add(a)

    def info_missing(self):
        missing = []
        for a in self._attr_names:
            if not getattr(self,a,None):
                missing.append(a)
        return missing

    def lookup_acoustic_id(self, missing_info):
        print("Calling acousticid for %s" % self.file)
        try:
            results = acoustid.match(acoustic_key, self.file)
            # Sort based on confidence interval (choose highest)
            sorted_result = sorted(results, key=lambda x: x[0], reverse=True)
        except Exception as exp:
            print("Error %s" % str(exp))
            return
        
        result = sorted_result[0]
        title = result[2]
        artist = result[3]
        if 'title' in missing_info:
            self.title = title
        elif self.title != title:
            self.title = '%s (%s)' % (self.title, title)
        if 'artist' in missing_info:
            self.artist = artist
        elif self.artist != artist:
            self.artist = '% (%s)' % (self.artist, artist)

    def to_tuple(self):
        return tuple(getattr(self,i,'') for i in self._attr_names)


if __name__ == '__main__':
    pwd = os.getcwd()
    files = [f for f in os.listdir() if os.path.isfile(f)]
    data = []

    acoustic_key = None
    homedir = os.path.join(os.path.expanduser('~'), '.acousticid-key.txt')
    with open(homedir) as fin:
        acoustic_key = fin.read()

    for f in files:
        ext = os.path.splitext(f)[1][1:]
        if ext not in AudioFormat:
            continue
        song = Song(pwd, f, acoustic_key=acoustic_key)
        data.append(song.to_tuple())
    data_sorted = sorted(data, key=lambda x: x[0])

    columns = ['Track', 'Artist', 'Year', 'Album', 'Song', 'Format']
    print(humanfriendly.tables.format_smart_table(data_sorted, columns))

