import json

def get_checklist(checklists, name=None):
    if name:
        for checklist in checklists:
            if checklist.get('name') == name:
                return checklist['checkItems']
    return checklists[0]['checkItems']

def convert(fin, checklist_name=None):
    with open(fin, 'r+') as f:
        json_data = json.loads(f.read())
        checklists = json_data['checklists']
        checklist = get_checklist(checklists, checklist_name)
        for item in checklist:
            yield "- [%s] %s" % ("X" if item.get('state') == 'complete' else " ",
                                 item.get('name').encode('utf8'))

def write_markdown(fname, fout_name, checklist_name=None):
    with open(fout_name, 'w+') as fout:
        for line in convert(fname, checklist_name):
            fout.write("%s\n" % line)

if __name__ == '__main__':
    import argparse
    import pprint
    import sys

    parser = argparse.ArgumentParser(description='Convert trello checklist to markdown file')
    parser.add_argument('-f', '--fname', required=True, type=str)
    parser.add_argument('-n', '--checklist-name', type=str)
    kwargs = vars(parser.parse_args())

    fname = kwargs.get('fname')
    fout_name = "%s.md" % (fname.split('.')[0])
    checklist_name = kwargs.get('checklist_name')

    write_markdown(fname, fout_name, checklist_name)

