#!/usr/bin/env python
import os
import sys
import base64
import subprocess
import tarfile
import getpass

import gnupg

utf8 = 'utf8'

def S(s):
    return s.encode(utf8)

def _decode_filename(filename):
    parts = filename.split('.')
    if len(parts) < 3:
        raise IndexError("Filename format foo.extension.gpg")
    if parts[-2] == 'gz':
        e_fname = S('.'.join(pargs[:-4]))
        ext = S(parts[-4])
    e_fname = S('.'.join(parts[:-2]))
    fname = base64.urlsafe_b64decode(e_fname)
    ext = S(parts[-2])
    return (fname, ext)

def _decrypt_file(filename, passphrase):
    """
    filename refers to the name of the file including the extension
    fname refers to the name of the file without the extension
    file refers to the path + filename of the file
    """
    fname, ext = _decode_filename(filename)
    gpg_dir = os.path.join(os.path.expanduser('~'), '.gnupg')
    gpg = gnupg.GPG(homedir=gpg_dir)
    current_dir = os.getcwd()
    e_file = os.path.join(current_dir, filename)
    fout_fname = b'%s.%s' % (fname, ext)
    plaintext_file = os.path.join(current_dir, fout_fname.decode('utf8'))

    with open(e_file, 'rb') as fin:
        data = fin.read().decode('utf8')

    result = gpg.decrypt(data,
                         passphrase=passphrase,
                         output=plaintext_file)
    if not result.ok:
        return result.stderr
    return "λ %s -> %s" % (filename, fout_fname.decode('utf8'))
   
def unstash(filename, passphrase):
    """
    If file:
    - gpg decrypt file
    - base64 decode filename

    If folder:
    - base64 decode folder name
    - extract folder
    - recursivly gpg decrypt files
    - recursivly base64 decode file names
    """
    result = _decrypt_file(filename, passphrase)
    print(result)
    if result.startswith('λ'):
        os.remove(filename)

if __name__ == '__main__':
    filename = sys.argv[1]
    passphrase = getpass.getpass(prompt='Enter passphrase: ')
    unstash(filename, passphrase)

