# The Following is ment to be appended to the bottom of your .bashrc file


# Add local bin folder (to which most of cmd-scripts should be symlinked to)
export PATH=$PATH:/usr/local/bin
if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi


# Bash Prompt with git branch support and colors
# Via https://coderwall.com/p/fasnya/add-git-branch-name-to-bash-prompt
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\[\033[33m\][\[\033[32m\]\w\[\033[33m\]]\[\033[33m\]\$(parse_git_branch)\[\033[00m\] \$ "

# Fixes odd paste issue I sometimes get
alias fix='printf "\e[?2004l"'

