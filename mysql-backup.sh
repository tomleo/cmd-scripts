#!/bin/bash

PATH=/bin:/usr/bin
export PATH

if [ ! -f "$CONFIG_FILE" ]; then
    echo "$CONFIG_FILE does not exist or is not a plain file!" >&2
    exit 1
fi

DESTFILE="$HOME/backup-${date +%u}.sql.gpg"

get_config_value() {
    cat "$CONFIG_FILE" | python -c "import sys, json; print json.load(sys.stdin)[\"$1\"]"
}

DB_HOST=$(get_config_value "DB_HOST")
DB_PORT=$(get_config_value "DB_PORT")
DB_USER=$(get_config_value "DB_USER")
DB_PASSWORD=$(get_config_value "DB_PASSWORD")

mysqldump -u"$DB_USER" \
          -p"$DB_PASSWORD" \
          --host="$DB_HOST" \
          --port="$DB_PORT" \
          --all-databases \
          --single-transaction \
          --events \
          --quick | \
    gpg --encrypt --recipient "$ADMIN_GPG_EMAIL" --yes --trust-model always > DESTFILE


