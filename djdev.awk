# This tool is for filtering django development logs to only show line number
# and message
#
# Requirements:
# sudo apt install expect
#
# Usage:
# unbuffer tail -f local_es.log | awk -f djdev.awk
#
/^DEBUG|INFO|WARNING|ERROR|CRITICAL/ {
    printf $6 ++ ": "
    print substr($0, index($0,$7))
}
