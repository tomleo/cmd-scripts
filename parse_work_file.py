import os
import datetime
import pytoml as toml

# Toml Format
# +++
# tags = [
# ]
# categories = [
# ]
# date = "$ISO_DATE"
# title = "$TITLE"
# 
# +++

def toml_in(string):
    return toml.load(string)

def toml_out(string):
    return toml.dumps(string)

def get_end_time(fname):
    end_time = datetime.datetime.fromtimestamp(os.path.getmtime(fname))
    return end_time.strftime('%I:%M:%S %p')

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Parse work document')



