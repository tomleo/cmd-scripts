#!/usr/bin/env python3
import os
import sys
import cbox
from colorama import Fore, Back, Style, init, deinit

END = '\033[0m'

def ext_to_icon_name(ext):
    pdf = ['.pdf']
    doc = ['.doc', '.docx']

'⛯' # U+26EF
'✉' # U+2709
'✓' # U+2713
'✕' # U+2715
'✗' # U+2717
'𝅘𝅥𝅮' # U+1D160

# Wide Chars
'🎶' # U+1F3B6
'🐍' # U+1F40D
'📃' # U+1F4C3a
'📆' # U+1F4C6
'📈' # U+1F4C8
'📉' # U+1F4C9
'📊' # U+1F4CA
'📋' # U+1F4CB
'🔒' # U+1F512
'🖂' # U+1F582

# U+1F150 to U+1F169 == (A) to (Z)
# U+1F170 to U+1F189

ICON_LOOKUP = {
    'doc': '🗋',
    'pdf': '🖹',
    'pic': '📸',
    'spreadsheet': '◴'
    'db': '⛁'  # U+26C1
}

@cbox.cmd
def ls(a=False):
    """A python implementation of the ls command
    
    :param a: Show hidden files
    """
    # TODO: only hide what's in .gitignore
    items = os.listdir()
    items = sorted(items, key=lambda x: x.lower())
    for item in items:
        item = item.strip()
        if not a and item.startswith('.'):
            continue
        
        if os.path.isfile(item):
            sys.stdout.write('%s %s\n' % (ICON_LOOKUP['doc'], item))
        else:
            sys.stdout.write(Style.BRIGHT + './' + END)
            sys.stdout.write(item + '\n')

if __name__ == '__main__':
    cbox.main(ls)

